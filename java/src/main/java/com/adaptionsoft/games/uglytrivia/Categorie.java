package com.adaptionsoft.games.uglytrivia;

public enum Categorie {

    POP(),
    SCIENCE(),
    SPORT(),
    ROCK(),
    TECHNO();

}
