package com.adaptionsoft.games.uglytrivia;

import java.util.HashMap;
import java.util.Map;

public class Player {

    private String name;
    private int place;
    private int purse;
    private boolean inPenaltyBox;
    private boolean hasJoker;
    private int winStreak;
    private int penalityCount;
    private boolean finished = false;
    private double penaltyOutChances = 0;

    private HashMap<String, Integer> stats;

    public Player(int place, int purse, boolean inPenaltyBox,String name) {
        this.name = name;
        this.place = place;
        this.purse = purse;
        this.inPenaltyBox = inPenaltyBox;
        this.hasJoker = true;
        this.winStreak = 0;
        this.penalityCount = 0;
        stats = new HashMap<>();
        stats.put("Pop", 0);
        stats.put("Science", 0);
        stats.put("Sports", 0);
        stats.put("Techno", 0);
        stats.put("Rock", 0);
    }

    public Player(String name) {
        this.place = 0;
        this.purse = 0;
        this.inPenaltyBox = false;
        this.name = name;
        this.hasJoker = true;
        this.winStreak = 0;
        this.penalityCount = 0;
        stats = new HashMap<>();
        stats.put("Pop", 0);
        stats.put("Science", 0);
        stats.put("Sports", 0);
        stats.put("Techno", 0);
        stats.put("Rock", 0);
    }


    public void addPenality(){
        this.penalityCount++;
    }

    public void restart(){
        this.place = 0;
        this.purse = 0;
        this.inPenaltyBox = false;
        this.hasJoker = true;
        this.winStreak = 0;
        this.penalityCount = 0;
        stats.put("Pop", 0);
        stats.put("Science", 0);
        stats.put("Sports", 0);
        stats.put("Techno", 0);
        stats.put("Rock", 0);
    }

    public boolean getFinished() {
        return this.finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public int getPenality(){
        return this.penalityCount;
    }

    public void setName(String name){
        this.name = name;
    }

    public void addCategorie(String categorString){
        this.stats.put(categorString, this.stats.get(categorString) + 1);
    }

    public void seeStats(){
        for(Map.Entry<String, Integer> entry : stats.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println("Categorie : " + key + "// Nb questions : " + value);
        }
    }

    public void hasGoodAnswer(){
        this.winStreak++;
    }

    public int getWinStreak(){
        return this.winStreak;
    }

    public void hasWrongAnswer(){
        this.winStreak = 0;
    }

    public String toString(){
        return name;
    }

    public String getName(){
        return name;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public int getPurse() {
        return purse;
    }

    public void setPurse(int purse) {
        this.purse = purse;
    }

    public boolean isInPenaltyBox() {
        return inPenaltyBox;
    }

    public void setInPenaltyBox(boolean inPenaltyBox) {
        this.inPenaltyBox = inPenaltyBox;
    }

    public void setHasJoker(boolean joker){
        this.hasJoker = joker;
    }

    public boolean getHasJoker(){
        return hasJoker;
    }

    public double getPenalityOutChance(){
        return this.penaltyOutChances;
    }

    public void addPenalityOutChance(double p){
        this.penaltyOutChances += p;
    }

}
