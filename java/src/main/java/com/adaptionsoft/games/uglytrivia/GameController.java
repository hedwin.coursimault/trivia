package com.adaptionsoft.games.uglytrivia;

import java.util.Random;
import java.util.Scanner;

public class GameController {
    

    private Game game;
    private boolean isStarted;

    public GameController(Game game){
        this.game = game;
        this.isStarted = false;
    }


    // Permet de commencer une game
    // Renvoie false si la game ne peut pas commencer true sinon
    public boolean start(){
        if(game.isPlayable()){
            this.isStarted = true;
            System.out.println("Game will start");
            gameRun(false);
        }else{
            System.out.println("Number of player is not allowed. Make sure there are between 2 and 6 players");
        }
        return this.isStarted;
    }

    public void gameRun(boolean restart){
        if(!restart) {
            askTechno();
            askNbGold();
        }
        
        Random rand = new Random();
		do {
                Player currentPlayer = game.getCurrentPlayer();
                String currentPlayerName = currentPlayer.getName();
                boolean didntWin = game.roll(rand.nextInt(5) + 1);
                if(!didntWin) {
                    System.out.println(currentPlayerName + " has finished !");
                    currentPlayer.setFinished(true);
                }
                System.out.println("-------");
                
		} while (game.getLeaderboard().size() < game.getPlayers().size() && game.getLeaderboard().size() < 3);

        System.out.println("\n  --- La partie est terminée ! ---  ");

        System.out.println("\n  --- LEADERBOARD ---  ");

        int count = 1;
        for(Player player : game.getLeaderboard()) {
            System.out.println(Integer.toString(count) + "  -  " + player);
            count++;
        }

        System.out.println("  ------------------  ");

        System.out.println("\n  --- Do you want to replay the same game ? y/n ---  ");

        Scanner sc = new Scanner(System.in);
        
        String userResponse = sc.nextLine();

        if(userResponse.equals("y")) {
            this.game.reset();
            this.gameRun(true);
        }

    }

    private void askTechno(){
        Scanner sc = new Scanner(System.in);
        String nextLine = "";

        while(!nextLine.equals("y") && !nextLine.equals("n") && !nextLine.equals("yes") && !nextLine.equals("no")){
            System.out.println("Do you want to replace Rock questions by techno questions for this game ? y/n");
            nextLine = sc.nextLine();
        }
        
        if(nextLine.equals("y") || nextLine.equals("yes")){
            System.out.println("Rock questions have been replaced by Techno questions");
            game.setIsReplacedByTechno(true);
        }

    }

    private void askNbGold(){
        Scanner sc = new Scanner(System.in);
        String nextLine = "";
        boolean isGood = false;
        int coins = 0;
        while(!isGood){
            System.out.println("Give coins value to win (default 6)");
            nextLine = sc.nextLine();
            try {
                coins = Integer.parseInt(nextLine);
                if(coins < 6 ){
                    System.out.println("Value can't be under 6");
                }else{
                    System.out.println(coins + " coins to win");
                    isGood = true;
                    this.game.setCoins(coins);
                }
            } catch (NumberFormatException e) {
                System.out.println("Default value is kept : 6 coins to win");
            }
        }
        
        if(nextLine.equals("y") || nextLine.equals("yes")){
            System.out.println("Rock questions have been replaced by Techno questions");
            game.setIsReplacedByTechno(true);
        }

    }

}
