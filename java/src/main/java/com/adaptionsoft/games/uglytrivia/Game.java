package com.adaptionsoft.games.uglytrivia;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class Game {
    ArrayList<Player> players = new ArrayList<Player>();
	int maxPlayer = 6;
	int minPlayer = 2;
	boolean isReplacedByTechno = false;
	int coins = 6;
	boolean forceTheme = false;
	Categorie nextCategorie;
	ArrayList<Player> leaderboard = new ArrayList<>();
    
    LinkedList<String> questions = new LinkedList<String>();
    
    int currentPlayer = 0;
    boolean isGettingOutOfPenaltyBox;
    
    public  Game(){
    	
    }

	public void reset() {
		leaderboard.clear();
		nextCategorie = null;
		forceTheme = false;
		ArrayList<Player> newPlayers = new ArrayList<Player>();
		for(Player player : this.players) {
			newPlayers.add(new Player(player.getName()));
		}
		this.players = newPlayers;
	}

	public void setIsReplacedByTechno(boolean b){
		this.isReplacedByTechno = b;
	}

	public String createQuestion(String theme, int index){
		return theme + " Question " + index;
	}

	public ArrayList<Player> getPlayers() {
		return this.players;
	}
	
	public boolean isPlayable() {
		return (howManyPlayers() >= this.minPlayer && howManyPlayers() <= this.maxPlayer);
	}

	public boolean add(String playerName) {
		if(players.size()<6){
			players.add(new Player(playerName));
			
			System.out.println(playerName + " was added");
			System.out.println("They are player number " + players.size());
			return true;
		}else{
			players.add(new Player(playerName));
			return false;
		}
	    
	}

	public Player getCurrentPlayer(){
		return players.get(currentPlayer);
	}

	public void removePlayer(Player p){
		players.remove(p);
	}

	
	public int howManyPlayers() {
		return players.size();
	}

	//retourne true si le match doit continuer
	//roll = nombre que tu as tiré
	public boolean roll(int roll) {
		
		if(this.isPlayable()){
			Player cPlayer = players.get(currentPlayer);
			while(cPlayer.getFinished()) {
				currentPlayer = (currentPlayer + 1) % players.size();
				cPlayer = players.get(currentPlayer);
			}
			System.out.println(Colors.ANSI_BLUE + cPlayer + Colors.ANSI_RESET + " is the current player");
			Scanner sc = new Scanner(System.in);
            String nextLine = "";
            
        	System.out.println("Do you want to Ragequit (type rq)");
             
            nextLine = sc.nextLine();
            if(nextLine.equals("rq")){
                System.out.println(cPlayer.getName() + " à quitté la partie");
                this.removePlayer(cPlayer);
				if (currentPlayer >= players.size()) currentPlayer = 0;
            }else{
				System.out.println("They have rolled a " + roll);
			
				if (cPlayer.isInPenaltyBox()) {
					Double d = Double.valueOf(cPlayer.getPenality());
					double chanceToLeavePrison = (1.0/d) + (cPlayer.getPenalityOutChance() / 100) ;

					System.out.println(Colors.ANSI_BLUE + "Chance de sortir de prison : + " + cPlayer.getPenalityOutChance() + "%");

					Random rand = new Random();
					double val = rand.nextDouble();

					System.out.println("VALLLL : " + val);
					System.out.println("chance to leave prison : " + chanceToLeavePrison +'\n');

	
					if (val <= chanceToLeavePrison) {
						isGettingOutOfPenaltyBox = true;
						cPlayer.setInPenaltyBox(false);
						System.out.println(Colors.ANSI_BLUE + cPlayer + Colors.ANSI_RESET + " is getting out of the penalty box");
						cPlayer.setPlace(cPlayer.getPlace() + roll);
						if (cPlayer.getPlace() > 11) cPlayer.setPlace(cPlayer.getPlace() - 12);
						
						return questionProcess();
					} else {
						System.out.println(Colors.ANSI_BLUE + cPlayer + Colors.ANSI_RESET + " is not getting out of the penalty box");
						isGettingOutOfPenaltyBox = false;
						cPlayer.addPenalityOutChance(10);
						
						currentPlayer++;
						if (currentPlayer >= players.size()) currentPlayer = 0;
					}
				
				} else {
					
					cPlayer.setPlace(cPlayer.getPlace() + roll);
					if (cPlayer.getPlace() > 11) cPlayer.setPlace(cPlayer.getPlace() - 12);
					
					return questionProcess();

				}
			}	
		}else{
			System.out.println("Il n'y a plus assez de joueur pour jouer");
			return false;
		}
		return true;
		
		
	}

	private boolean questionProcess(){
		Scanner sc = new Scanner(System.in);
		String nextLine = "";
		Player cPlayer = players.get(currentPlayer);
		System.out.println(Colors.ANSI_BLUE + players.get(currentPlayer) + Colors.ANSI_RESET
							+ "'s new location is " 
							+ cPlayer.getPlace());
		String cat = currentCategory();
		System.out.println("The category is " + cat);
		questions.add(createQuestion(cat, questions.size()));
		System.out.println(questions.getLast());
		
		//askQuestion();
		if(cPlayer.getHasJoker()){
			System.out.println("Do you want to use joker ? (Type jk)");
			nextLine = sc.nextLine();
			if(nextLine.equals("jk")){
				System.out.println("You have used your joker");
				cPlayer.setHasJoker(false);
				return useJoker();
			}
		}
		System.out.println("Input your answer");
		nextLine = sc.nextLine();
		if(isGoodAnswer(nextLine)){
			return wasCorrectlyAnswered();
		}else{
			return wrongAnswer();
		}
	}


	// private void askQuestion() {
	// 	System.out.println(questions.getLast());	
	// }

	private boolean isGoodAnswer(String a){
		int rep = 7;
		try {
			rep = Integer.parseInt(a);
		} catch (NumberFormatException e) {
			return false;
		}
		if (rep == 7) {
			return false;
		} else {
			return true;
		}
	}
	
	
	private String currentCategory() {
		Player cPlayer = players.get(currentPlayer);
		if(forceTheme){
			forceTheme = false;
			
			if(nextCategorie == Categorie.POP){
				cPlayer.addCategorie("Pop");
				return "Pop";
			}
			if(nextCategorie == Categorie.SCIENCE){
				cPlayer.addCategorie("Science");
				return "Science";
			}
			if(nextCategorie == Categorie.SPORT){
				cPlayer.addCategorie("Sports");
				return "Sports";
			}
			if(nextCategorie == Categorie.TECHNO){
				cPlayer.addCategorie("Techno");
				return "Techno";
			}
			if(nextCategorie == Categorie.ROCK){
				cPlayer.addCategorie("Rock");
				return "Rock";
			}
			return "Rock";
		}else{
			Random rand = new Random();
			int categorieNumber = rand.nextInt(4) + 1;
			if(categorieNumber == 1){
				cPlayer.addCategorie("Pop");
				return "Pop";
			}
			if(categorieNumber == 2){
				cPlayer.addCategorie("Science");
				return "Science";
			}
			if(categorieNumber == 3){
				cPlayer.addCategorie("Sports");
				return "Sports";
			}
			if(categorieNumber == 4 && isReplacedByTechno){
				cPlayer.addCategorie("Techno");
				return "Techno";
			}
			if(categorieNumber == 4 && !isReplacedByTechno){
				cPlayer.addCategorie("Rock");
				return "Rock";
			}
			return "Rock";
		}
		
	}

	//Déclancher l'utilisation d'un joker
	public boolean useJoker(){
		Player cPlayer = players.get(currentPlayer);
		boolean winner = didPlayerWin();
				cPlayer.hasWrongAnswer();
				currentPlayer++;
				if (currentPlayer == players.size()) currentPlayer = 0;
				
				return !winner;
	}

	//Déclancher une bonne réponse
	public boolean wasCorrectlyAnswered() {
		Player cPlayer = players.get(currentPlayer);
		cPlayer.hasGoodAnswer();
		if (cPlayer.isInPenaltyBox()){
			if (isGettingOutOfPenaltyBox) {
				System.out.println("Answer was correct!!!!");
				
				cPlayer.setPurse(cPlayer.getPurse()+cPlayer.getWinStreak());
				System.out.println(players.get(currentPlayer) 
						+ " now has "
						+ cPlayer.getPurse()
						+ " Gold Coins.");
				
				boolean winner = didPlayerWin();
				currentPlayer++;
				if (currentPlayer == players.size()) currentPlayer = 0;
				
				return !winner;
			} else {
				currentPlayer++;
				if (currentPlayer == players.size()) currentPlayer = 0;
				return true;
			}
			
			
			
		} else {
		
			System.out.println("Answer was corrent!!!!");
			cPlayer.setPurse(cPlayer.getPurse()+cPlayer.getWinStreak());
			System.out.println(players.get(currentPlayer) 
					+ " now has "
					+ cPlayer.getPurse()
					+ " Gold Coins.");
			
			boolean winner = didPlayerWin();
			currentPlayer++;
			if (currentPlayer == players.size()) currentPlayer = 0;
			
			return !winner;
		}
	}

	//Déclancher une erreur fausse
	public boolean wrongAnswer(){
		Player cPlayer = players.get(currentPlayer);
		cPlayer.hasWrongAnswer();
		System.out.println("Question was incorrectly answered");
		System.out.println(Colors.ANSI_BLUE + players.get(currentPlayer) + Colors.ANSI_RESET + " was sent to the penalty box");
		cPlayer.setInPenaltyBox(true);
		cPlayer.addPenality();
		askForNextQuestion();
		currentPlayer++;
		if (currentPlayer == players.size()) currentPlayer = 0;
		return true;
	}

	public void askForNextQuestion(){
		Scanner sc = new Scanner(System.in);
		String nextLine = "";
		boolean retry = true;
		while(retry){
			System.out.println("Choose theme for next question");
			System.out.println("||1.Pop                     ||");
			System.out.println("||2.Science                 ||");
			System.out.println("||3.Sport                   ||");
			System.out.println("||4.Rock / Techno           ||");
			nextLine = sc.nextLine();
			int val = 0;
			try {
				val = Integer.parseInt(nextLine);
			} catch ( NumberFormatException e) {
				System.out.println("Wrong input retry");
			}
			retry = false;
			this.forceTheme = true;
			switch (val) {
				case 1:
					nextCategorie = Categorie.POP;
					break;
				case 2:
					nextCategorie = Categorie.SCIENCE;
					break;
				case 3:
					nextCategorie = Categorie.SPORT;
					break;
				case 4:
					if(isReplacedByTechno){
						nextCategorie = Categorie.TECHNO;
					}else{
						nextCategorie = Categorie.ROCK;
					}
					break;

				default:
					System.out.println("Wrong input retry");
					retry = true;
					this.forceTheme = false;
					break;
			}
		}
		System.out.println("Next theme : " + nextCategorie.name());
		
	}


	///retourne true si le player a gagné
	public boolean didPlayerWin() {
		Player cPlayer = players.get(currentPlayer);
		if(cPlayer.getPurse() == coins || players.size() == 1){
			this.leaderboard.add(cPlayer);
			return true;
		}else{
			return false;
		}

	}

	public ArrayList<Player> getLeaderboard(){
		return this.leaderboard;
	}


	public void setCoins(int coins){
		this.coins = coins;
	}
}


