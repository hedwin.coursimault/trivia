
package com.adaptionsoft.games.trivia.runner;
import com.adaptionsoft.games.uglytrivia.GameController;


public class GameRunner {

	public static void main(String[] args) {

		GameCreator gameCreator = new GameCreator();
		gameCreator.setPlayers();

		GameController gameController = new GameController(gameCreator.getGame());
		gameController.start();
		
		
	}
}
