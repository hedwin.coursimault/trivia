package com.adaptionsoft.games.trivia.runner;

import java.util.Scanner;

import com.adaptionsoft.games.uglytrivia.Game;

/**
 * La classe est utilisé pour ajouter des joueurs via le terminal
 */
public class GameCreator {
    
    private Game game;

    public GameCreator(){
        this.game = new Game();
    }

    /**
     * Permet d'ajouter des joueurs
     */
    public void setPlayers(){
        boolean isAddingPlayers = true;
        Scanner scanner = new Scanner(System.in);
        while(isAddingPlayers){
            System.out.println("Ajouter un joueur : ");
            try{
                String player = scanner.nextLine();
                if(player.equals("STOP") || player.equals("") || player.equals(" ") || player.equals("stop")){
                    isAddingPlayers = false;
                }else{
                    game.add(player);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Permet de récupérer la partie avec les joueurs ajoutés
     * @return
     */
    public Game getGame(){
        return game;
    }
    
}