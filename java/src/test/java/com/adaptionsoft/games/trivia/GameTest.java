package com.adaptionsoft.games.trivia;

import static org.junit.Assert.*;

import com.adaptionsoft.games.uglytrivia.Game;
import com.adaptionsoft.games.uglytrivia.GameController;

import org.junit.Test;

public class GameTest {

	@Test
	public void players_number() throws Exception {
		Game g = new Game();
		GameController gc = new GameController(g);

		g.add("DwinDwin");
		assertFalse("Players number should return unplayable game", gc.start());

		g.add("Tom");
		assertTrue("Players number should return playable game", gc.start());

		for (int i = 0; i < 3; i++) {
			String s = "Player_" + i;
			g.add(s);
		}
		assertTrue("Players number should return playable game", gc.start());
	}


	@Test(expected = IndexOutOfBoundsException.class)
	public void players_number_expectFail() throws Exception {
		Game g = new Game();
		GameController gc = new GameController(g);

		for (int i = 0; i < 10; i++) {
			String s = "Player_" + i;
			g.add(s);
		}

		assertFalse("Players number should return unplayable game ",gc.start());
	}


}
